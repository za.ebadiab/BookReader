package basic.android.bookreader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import static basic.android.bookreader.PublicMethodVariables.imagesIdArr;

public class BookFragment extends Fragment {


    ImageView imgPage;
    private static BookFragment bookFragment;


    public static BookFragment getFragment(int position) {
        //    if (bookFragment == null) {
        bookFragment = new BookFragment();
        //  }

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bookFragment.setArguments(bundle);

        return bookFragment;

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.book_fragment_item, container, false);

        imgPage = v.findViewById(R.id.imgPage);
        imgPage.setImageBitmap(getPageFromDrawable(imagesIdArr[getArguments().getInt("position", 0)]));

        return v;
    }


    public Bitmap getPageFromDrawable(int i) {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), i);
        return bitmap;
    }

}
