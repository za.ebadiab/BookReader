package basic.android.bookreader;


import android.support.v4.view.ViewPager;
import android.os.Bundle;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

public class MainActivity extends BaseActivity {

    ViewPager pager;
    SmartTabLayout viewpagertabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        setPagerAdapter();

        setTabs();

    }




    void bind() {

        pager = findViewById(R.id.pager);
        viewpagertabs = findViewById(R.id.viewpagertabs);


    }


    void setPagerAdapter() {
        BookAdapter bookAdapter = new BookAdapter(getSupportFragmentManager());
        pager.setAdapter(bookAdapter);
        pager.setOffscreenPageLimit(3);

    }
    void setTabs(){
        viewpagertabs.setViewPager(pager);

    }

}
