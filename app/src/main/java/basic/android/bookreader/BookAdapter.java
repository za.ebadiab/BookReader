package basic.android.bookreader;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;


//public class BookAdapter extends FragmentPagerAdapter {
public class BookAdapter extends FragmentStatePagerAdapter {

    public BookAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        Log.d("my log", "in adapter -> getItem position = " + position);

        return BookFragment.getFragment(position);

    }

    @Override
    public int getCount() {

         return PublicMethodVariables.imagesIdArr.length;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "" + (position + 1);
    }

}
